<?php
	// Connect to database
	include("db_connect.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	function getTickets()
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function getTicketById($id=0)
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		if($id != 0)
		{
			$query .= " WHERE id=".$id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		
		if (count($response)==0){
		  header("HTTP/1.0 404 Not Found");
		  return ;
		}
		
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function AddTicket()
	{
		global $conn;
		$date = $_POST["date"];
		$probleme = $_POST["probleme"];
		$severite = $_POST["severite"];
		$query="INSERT INTO ticket(date, probleme, severite) VALUES('".$date."', '".$probleme."', '".$severite."')";
		
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Le ticket a �t� ajout� avec succ�s.'
			);
			header('Content-Type: application/json');
		  echo json_encode($response, JSON_PRETTY_PRINT);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'ERREUR!.'. mysqli_error($conn)
			);
			header('Content-Type: application/json');
		  echo json_encode($response, JSON_PRETTY_PRINT);
		}
	}
	
	function updateTicket($id)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$date = $_POST["date"];
		$probleme = $_POST["probleme"];
		$severite = $_POST["severite"];
		$category = $_POST["category"];
		$query ="UPDATE ticket SET date='".$date."', probleme='".$probleme."', severite='".$severite."' WHERE id=".$id;
		
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket mis � jour avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise � jour du ticket. '. mysqli_error($conn)
			);
			
		}
		
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function deleteTicket($id)
	{
	  header('Content-Type: application/json');
		global $conn;
		$query = "DELETE FROM ticket WHERE id=".$id;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket supprim� avec succes.'
			);
			header("HTTP/1.0 200 Ok");
			echo json_encode($response);
			
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du ticket a echoue. '. mysqli_error($conn)
			);
			header("HTTP/1.0 404 Not Found");
		  echo json_encode($response);
		}
	}

	switch($request_method)
	{	
		case 'GET':
		  if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				getTicketById($id);
			}
			else
			{
				getTickets();
			}
			break;
			
	  case 'POST':
			// Ajouter un ticket
			AddTicket();
			break;
			
	  case 'PUT':
			  // Modifier un ticket
			  $id = intval($_GET["id"]);
			  updateProduct($id);
			  break;
	  
	  
	  case 'DELETE':
		  if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				deleteTicket($id);
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

	}
?>
